# MLCL LIFE INSURANCE AUTOMATION SCREENING TEST

This project has been created to cater for three test exercises given as part of MLC Life Insurance Automation Screening test.
This project was created as a maven project and is using the Serenity BDD framework. Below are instructions/ guidelines to import the project,run the tests, generate report and some troubleshooting steps for frequently faced issues.

# Machine requirements or other Pre-Requisites:
1. Maven and JAVA installed and setup
2. Any IDE (IntelliJ or Eclipse)
3. Before running the API Test, you will have to register with Australia Post Developer Centre in order to generate an API access key. 
   Update the "ACCESS_KEY" variable in APIConstants.java present at   "\src\test\java\mlc\life\insurance\Automation\Assessment\apiRequestResponse"


# Steps to import the project:
1. Import the project as Maven project in your local IDE.
2. Resolve all the dependencies in pom.xml file(You can use maven plugin in IDE/ by running "mvn clean install" in command prmopt after    navigationg to the pom file location)

# Important file locations:
Test Runner     : \src\test\java\mlc\life\insurance\Automation\Assessment\testRunner
Feature Files   : \src\test\java\mlc\life\insurance\Automation\Assessment\features
Step Definition : \src\test\java\mlc\life\insurance\Automation\Assessment\stepDefinition
Web Drivers     : \src\test\resources\webdriver
serenity.conf   : \src\test\resources
Report          : \target\site\report


# Steps to run test cases:
1. If you are opening the project in IDE, navigate to the Test Runner class and uncomment the required tag and comment the remaining ones:
   
   To run all tests from all 3 exercises : @AllTests
   To run all tests for MLC LifeView Demo request: @WebUITestPart1
   To run all tests for ATO Tax Calculation : @WebUITestPart2
   To run all tests for Austpost International Package Cost Calculation API : @APITest

2. Currently, the browser to run the UI Tests is set as "Chrome", this can be changed in serenity.conf
   
3. Test cases can alternatively be run from command line using command : mvn clean verify -Dcucumber.options="--tags @AllTests"
   Change the tag in command to run the required test cases as mention in Step 1
   
4. Once the tests are complete, report will be generated in reports folder '\target\site\report' with a '.html' extension.
   
5. If all tests for 3 exercises are run together using '@AllTests' tag, three seperate reports will be generated with .html 
   tag under same reports folder. In order to aggregate all three reports in one report, use the below command:
   in command line after tests have completed successfully: ' mvn serenity:aggregate '

 
# Frequently faced issues:
 
 1. After the project has been imported successfully and the dependencies have been resolved, 
 the initial UI test cases sometime fail with error:'Could not instantiate new WebDriver instance of type class org.openqa.selenium.chrome.ChromeDriver'
 
    This might happen due to different version of Web driver in project folder and the version installed on machine. Please make sure both are of same version in case you face this issue for any of the drivers.
 
 2. Maven build might fail sometime while trying to resolve the dependency
 
    This might happen due to a breaking internet connection.
   