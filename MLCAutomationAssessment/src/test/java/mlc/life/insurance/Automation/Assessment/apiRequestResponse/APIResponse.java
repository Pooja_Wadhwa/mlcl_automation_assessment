package mlc.life.insurance.Automation.Assessment.apiRequestResponse;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

/*  This class is used to capture the elements from JSON response  */

public class APIResponse {

	@Step

	/*  This method is used to capture the TotalCost from JSON response  */
		public String getTotalPrice() {

		return SerenityRest.lastResponse().getBody().jsonPath().getString(APIConstants.JSON_PATH);

    }
}
