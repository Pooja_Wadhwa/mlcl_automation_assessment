package mlc.life.insurance.Automation.Assessment.testRunner;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = "src\\test\\java\\mlc\\life\\insurance\\Automation\\Assessment\\features",
        glue = {"mlc.life.insurance.Automation.Assessment.stepDefinition"},
        //tags = "@WebUITestPart1"
        //tags = "@WebUITestPart2"
        //tags = "@APITest"
        tags   = "@AllTests"
)

public class TestRunner {

}
