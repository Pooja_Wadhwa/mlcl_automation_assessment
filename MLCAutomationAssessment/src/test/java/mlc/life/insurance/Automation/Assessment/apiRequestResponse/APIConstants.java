package mlc.life.insurance.Automation.Assessment.apiRequestResponse;

/*  This class contains the constants used in REQUEST and STEPDEFINITION
    of API Assessment Test   */

public class APIConstants {
	 public static final String COUNTRY_CODE = "Country_Code";
	 public static final String WEIGHT = "Weight";
	 public static final String SERVICE_CODE = "Service_Code";
	 public static final String APPLICATION_JSON = "application/json";
	 public static final String USER_AGENT = "User-Agent";
	 public static final String CURL = "Curl";
	 public static final String AUTH_KEY = "AUTH-KEY";
	 public static final String ACCESS_KEY = "4899bab4-a045-4003-aa74-350ba908a7e6";
	 public static final String JSON_PATH = "postage_result.total_cost";
	 
}
