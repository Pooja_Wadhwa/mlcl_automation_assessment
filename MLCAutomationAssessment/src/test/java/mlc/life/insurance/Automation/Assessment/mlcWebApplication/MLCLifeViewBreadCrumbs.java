package mlc.life.insurance.Automation.Assessment.mlcWebApplication;

import java.util.List;
import java.util.stream.Collectors;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.steps.UIInteractionSteps;

public class MLCLifeViewBreadCrumbs extends UIInteractionSteps {
    public List<String> getBreadCrumbs() {
        return findAll(MLCWebElements.NAVIGATION_TITLES)
                .stream()
                .map(WebElementFacade::getText)
                .collect(Collectors.toList());	
    }
}
