package mlc.life.insurance.Automation.Assessment.mlcWebApplication;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;

public class MLCDemoRequestForm extends UIInteractionSteps {

	@Step
	public void fillDemoRequestForm(String name, String company, String email, String phone) {
		$(MLCWebElements.DEMO_FORM_NAME).clear();
		$(MLCWebElements.DEMO_FORM_NAME).type(name);
		$(MLCWebElements.DEMO_FORM_COMPANY).clear();
		$(MLCWebElements.DEMO_FORM_COMPANY).type(company);
		$(MLCWebElements.DEMO_FORM_EMAIL).clear();
		$(MLCWebElements.DEMO_FORM_EMAIL).type(email);
		$(MLCWebElements.DEMO_FORM_PHONE).clear();
		$(MLCWebElements.DEMO_FORM_PHONE).type(phone);
	}
}
