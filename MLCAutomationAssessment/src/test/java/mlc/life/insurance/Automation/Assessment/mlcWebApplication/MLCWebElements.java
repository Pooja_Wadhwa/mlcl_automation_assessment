package mlc.life.insurance.Automation.Assessment.mlcWebApplication;

import org.openqa.selenium.By;
/*  This class contains the locators for web elements used in MLC LifeView Demo Request */

class MLCWebElements {
    static By TOGGLE_SEARCH = By.xpath("//*[@id='nav-onscreen']/button");
    static By SEARCH_FIELD = By.cssSelector("#q");
    static By SEARCH_BUTTON = By.xpath("//*[@id='offscreen-search']//*[@class='icon svg-search-white']/*");
    static By SEARCH_RESULT_LIFEVIEW = By.xpath("//*[@id='main']//*[contains(@href,'partnering-with-us/superannuation-funds/lifeview')]");
    static By NAVIGATION_TITLES = By.xpath("//div[@class='breadcrumbs']//ul");
    static By DEMO_BUTTON = By.xpath("//*[contains(text(),'Request a demo')]");
    static By DEMO_FORM_NAME = By.xpath("//input[@id='wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_0__Value']");
    static By DEMO_FORM_COMPANY = By.xpath("//*[@id='wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_1__Value']");
    static By DEMO_FORM_EMAIL = By.xpath("//input[@id='wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_2__Value']");
    static By DEMO_FORM_PHONE = By.xpath("//input[@id='wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_3__Value']");
}
