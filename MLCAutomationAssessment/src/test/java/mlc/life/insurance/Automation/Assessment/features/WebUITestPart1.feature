Feature: MLC Life Insurance Lifeview UI automation

  @WebUITestPart1 @AllTests
  Scenario Outline: Web UI Test - Part 1 - Request a demo for LifeView
    Given User navigates to MLC homepage "https://www.mlcinsurance.com.au/"
    When User search for keyword "Lifeview"
    And Click on lifeview link
    Then Lifeview page is displayed with breadcrumbs on the top as "Home Partnering with us Superannuation funds LifeView"
    Then User Clicks on request a demo button
    And Enter relevant data in the form name as "<name>", company as "<company>", email as "<email>", phone as "<phone>"

    Examples:
      | name        | company    | email                | phone             |
      | DemoUser1   |  TestComp1 |  demouser@test.com   |   411111111       |
