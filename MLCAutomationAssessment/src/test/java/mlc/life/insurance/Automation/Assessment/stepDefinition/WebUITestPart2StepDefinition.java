package mlc.life.insurance.Automation.Assessment.stepDefinition;

import static org.assertj.core.api.Assertions.assertThat;

//import cucumber.api.DataTable;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class WebUITestPart2StepDefinition {
	public static String atoURL;
	public String actualTaxValues;

	@Steps
    mlc.life.insurance.Automation.Assessment.atoWebApplication.ATOUINavigation ATOUINavigation;

	@Given("^User navigates to ATO page \"([^\"]*)\"$")
	public void navigatetoATOPage(String url) {
		atoURL = url;
		ATOUINavigation.isOnATOPage(url);
	}
	
	@When("^User enters assessment year as \"([^\"]*)\", income as \"([^\"]*)\", residency as \"([^\"]*)\" and taxableMonths as \"([^\"]*)\"$")
	public void enterATODetails(String year,String income, String residency, String taxableMonths){
		ATOUINavigation.enterATODetails(year, income, residency, taxableMonths);
		actualTaxValues= ATOUINavigation.readTaxValue();
	}

	
	@Then("^Tax calculated for the user should be \"([^\"]*)\"$")
	public void verifyCalculatedTax(String expectedTaxAmount) {

		assertThat(expectedTaxAmount.trim()).containsIgnoringCase(actualTaxValues.trim().replace("$", ""));

	}
}
