package mlc.life.insurance.Automation.Assessment.stepDefinition;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.When;
import mlc.life.insurance.Automation.Assessment.mlcWebApplication.MLCGlobalSearch;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.en.And;

public class WebUITestPart1StepDefinition {

	@Steps
    mlc.life.insurance.Automation.Assessment.mlcWebApplication.MLCUINavigation MLCUINavigation;

	@Steps
    MLCGlobalSearch search;

	@Steps
    mlc.life.insurance.Automation.Assessment.mlcWebApplication.MLCDemoRequestForm MLCDemoRequestForm;

	@Given("^User navigates to MLC homepage \"([^\"]*)\"$")
	public void navigatetoMLCHome(String url) {
		MLCUINavigation.mlcHome(url);
	}

	@When("^User search for keyword \"([^\"]*)\"$")
	public void search_for(String keyword) {
		search.searchkeyword(keyword);
	}

	@And("^Click on lifeview link$")
	public void clickOnLifeviewLink() {
		MLCUINavigation.clickOnLifeviewLink();
	}

	@Then("^Lifeview page is displayed with breadcrumbs on the top as \"([^\"]*)\"$")
	public void verifyNavigationText(String breadCrumbs) {
		List<String> lifeViewNavigationTitlesList = new ArrayList<String>();
		lifeViewNavigationTitlesList = MLCUINavigation.getlifeViewNavigationTitlesList();
		for (int i = 0; i < lifeViewNavigationTitlesList.size(); i++) {
			assertThat(breadCrumbs).containsIgnoringCase(lifeViewNavigationTitlesList.get(i).trim());
		}
	}

	@Then("^User Clicks on request a demo button$")
	public void clickOnRequestDemoButton() {
		MLCUINavigation.clickOnDemoButton();
	}

	@And("^Enter relevant data in the form name as \"([^\"]*)\", company as \"([^\"]*)\", email as \"([^\"]*)\", phone as \"([^\"]*)\"$")
	public void enterFormData(String name,String company, String email, String phone) {
		MLCDemoRequestForm.fillDemoRequestForm(name, company, email, phone);
	}
}
