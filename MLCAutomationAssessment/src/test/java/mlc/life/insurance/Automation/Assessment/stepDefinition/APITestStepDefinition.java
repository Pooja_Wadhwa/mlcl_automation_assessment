package mlc.life.insurance.Automation.Assessment.stepDefinition;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import mlc.life.insurance.Automation.Assessment.apiRequestResponse.APIRequest;
import mlc.life.insurance.Automation.Assessment.apiRequestResponse.APIResponse;
import net.thucydides.core.annotations.Steps;

public class APITestStepDefinition {
	public static String apiEndPointURL;
	List<Map<String, String>> list = new ArrayList<Map<String, String>>();
	//public List <String> prices= new ArrayList<String>();
	public List <Object> prices= new ArrayList<Object>();
	public String totalCostFromResponse;

	@Steps
    APIRequest apiRequest;
	
	@Steps
    APIResponse apiResponse;

	@Given("^The API is up and running with endpoint \"([^\"]*)\"$")
	public void setEndPointURL(String url) {
		apiEndPointURL = url;
	}
	
	@When("^User sends request using \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\" parameters$")
	public void sendRequest(String weight, String country_code, String service_code) {

       		System.out.println(apiEndPointURL + "?" + "Weight=" + weight + "&Country_Code=" + country_code + "&Service_Code=" + service_code);
			apiRequest.priceRequest(weight,country_code,service_code, apiEndPointURL);
			lastResponse().prettyPrint();
			restAssuredThat(response -> response.statusCode(200));
	       	totalCostFromResponse = apiResponse.getTotalPrice();
			System.out.println(totalCostFromResponse);

	        }


	
	@Then("^User receives TotalCost as \"([^\"]*)\"$")
	public void parseResponse(String expectedTotalCost) {
		assertThat(expectedTotalCost.trim()).containsIgnoringCase(totalCostFromResponse.trim());
	}
}
