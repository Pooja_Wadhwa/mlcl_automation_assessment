package mlc.life.insurance.Automation.Assessment.atoWebApplication;

/*  This class contains the constants for ATO TAX CALCULATION SCENARIO    */

public class ATOConstants {
	public static final String YEAR = "Year";
	public static final String INCOME = "Income";
	public static final String RESIDENCY = "Residency";
	public static final String TAXABLE_MONTHS = "TaxableMonths";
	public static final String EXPECTED_AMOUNT = "ExpectedTaxAmount";
}
