package mlc.life.insurance.Automation.Assessment.mlcWebApplication;

import java.util.ArrayList;
import java.util.List;

import net.thucydides.core.annotations.Step;

public class MLCUINavigation {

    MLCHomePage mlcHomePage;
    MLCLifeViewBreadCrumbs MLCLifeViewBreadCrumbs;

    @Step
    public void mlcHome(String url)
    {

        mlcHomePage.openUrl(url);
    }
    
    @Step
    public void clickOnLifeviewLink()
    {

        mlcHomePage.$(MLCWebElements.SEARCH_RESULT_LIFEVIEW).click();
    }
    
    @Step
    public List<String> getlifeViewNavigationTitlesList()
    {
    	List<String> lifeViewNavigationTitlesList= new ArrayList<String>();  
    	lifeViewNavigationTitlesList = MLCLifeViewBreadCrumbs.getBreadCrumbs();
    	return lifeViewNavigationTitlesList;
    }
    
    @Step
    public void clickOnDemoButton() {
        mlcHomePage.$(MLCWebElements.DEMO_BUTTON).click();
    }
    
}
