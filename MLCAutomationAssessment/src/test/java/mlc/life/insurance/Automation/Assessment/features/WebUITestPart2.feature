Feature: Web UI Test - Part 2 - ATO Application

  @WebUITestPart2 @AllTests
  Scenario Outline: Web UI Test - Part 2 - ATO - Verify calculated tax
    Given User navigates to ATO page "https://www.ato.gov.au/Calculators-and-tools/Host/?anchor=STC&anchor=STC#STC/questions"
    When User enters assessment year as "<Year>", income as "<Income>", residency as "<Residency>" and taxableMonths as "<TaxableMonths>"
    Then Tax calculated for the user should be "<ExpectedTaxAmount>"

    Examples:
      | Year 		  | Income 					| Residency   					| TaxableMonths | ExpectedTaxAmount   |
      | 2018-19 	  | 86750 	 				| Resident for full year 		|     12		| 19,740.75			  |
      | 2017-18 	  | 105000 	 				| Non-resident for full year    |     12		| 34,935.00			  |
      | 2016-17 	  | 141400 	 				| Part-year resident 			|	  5		    | 40,474.78			  |



