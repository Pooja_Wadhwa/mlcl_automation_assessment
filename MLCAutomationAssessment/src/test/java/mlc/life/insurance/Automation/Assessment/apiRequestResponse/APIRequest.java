package mlc.life.insurance.Automation.Assessment.apiRequestResponse;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

/*  This class is used to trigger an API Request by passing the mandatory parameters for request  */

public class APIRequest {

    @Step
    public void priceRequest(String weight, String countryCode, String serviceCode, String url) {
    	SerenityRest.given()
    	.queryParams(APIConstants.COUNTRY_CODE, countryCode, APIConstants.WEIGHT, weight,APIConstants.SERVICE_CODE, serviceCode)
        .contentType(APIConstants.APPLICATION_JSON)
        .headers(APIConstants.USER_AGENT, APIConstants.CURL, APIConstants.AUTH_KEY, APIConstants.ACCESS_KEY)
        .when()
        .get(url);
    }
}
