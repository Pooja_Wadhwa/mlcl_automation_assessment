package mlc.life.insurance.Automation.Assessment.atoWebApplication;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;

public class ATOUINavigation {

	ATOPage atoPage;


	@Step
	public void isOnATOPage(String url) {
		atoPage.openUrl(url);
	}

	@Step
	public void enterATODetails(String year, String income, String residencyStatus, String taxableMonths) {

		//Selecting Assessment year
		WebElementFacade yearDropDown = atoPage.$(ATOUIWebElements.ASSESSMENT_YEAR);
		yearDropDown.selectByVisibleText(year);

		//Selecting Income
		atoPage.$(ATOUIWebElements.INCOME).type(income);

		//Selecting Residential status
		if (residencyStatus.equalsIgnoreCase("Resident for full year")) {
			atoPage.$(ATOUIWebElements.RESIDENT).click();
		} else if (residencyStatus.equalsIgnoreCase("Non-resident for full year")) {
			atoPage.$(ATOUIWebElements.NON_RESIDENT).click();
		} else {
			atoPage.$(ATOUIWebElements.PART_RESIDENT).click();
			WebElementFacade partYearMonths = atoPage.$(ATOUIWebElements.PART_YEAR_MONTHS);
			partYearMonths.selectByVisibleText(taxableMonths);
		}

		// submitting the page
		atoPage.$(ATOUIWebElements.SUBMIT_BUTTON).click();
	}
	
	@Step
	public String readTaxValue() {
		return atoPage.$(ATOUIWebElements.TAX_CALCULATED).getText();
	}

}
