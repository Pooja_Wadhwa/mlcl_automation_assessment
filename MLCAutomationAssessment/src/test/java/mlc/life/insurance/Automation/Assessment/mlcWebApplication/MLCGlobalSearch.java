package mlc.life.insurance.Automation.Assessment.mlcWebApplication;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;

public class MLCGlobalSearch extends UIInteractionSteps {

	@Step
	public void searchkeyword(String keyword) {
		$(MLCWebElements.TOGGLE_SEARCH).waitUntilEnabled().click();
		$(MLCWebElements.SEARCH_FIELD).clear();
		$(MLCWebElements.SEARCH_FIELD).typeAndEnter(keyword);
	}
}
