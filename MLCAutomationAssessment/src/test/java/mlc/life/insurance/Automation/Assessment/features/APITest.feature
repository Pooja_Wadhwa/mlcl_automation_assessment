Feature: Australia Post API to calculate shipping costs

  @APITest @AllTests
  Scenario Outline: Australia Post API to calculate shipping costs for parcels of different weights
    Given The API is up and running with endpoint "https://digitalapi.auspost.com.au/postage/parcel/international/calculate.json"
    When User sends request using "<Weight>", "<Country_Code>" and "<Service_Code>" parameters
    Then User receives TotalCost as "<ExpectedTotalCost>"

    Examples:
      | Weight 			  | Country_Code			| Service_Code                   |  ExpectedTotalCost   |
      | 0.5      	      | NZ 	 					| INT_PARCEL_EXP_OWN_PACKAGING   |  33.20               |
      | 3.0      	      | US 	 					| INT_PARCEL_COR_OWN_PACKAGING   |  141.50              |
      | 8.67       	      | GR 	 					| INT_PARCEL_SEA_OWN_PACKAGING   |  119.65              |


